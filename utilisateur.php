<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Epicerie Fine - Projet S5/s3</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="assets/img/theicon.jpg" rel="icon">
    <link href="assets/img/the-icon.jpg" rel="Solidele-touch-icon">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="assets/css/styles.css" rel="stylesheet">
    <style>
        body {
            top: 20%;
            width: 100%;
            /* background: linear-gradient(to right, #17594A, #65B741); */
            background-color: #fff;
            justify-content: center;
            align-items: center;

        }

        .box {
            width: 600px;
            padding: 40px;
            position: absolute;
            top: 10%;
            left: 47%;
            transform: translate(-50%, 20%);
            background: #191919;
            border-radius: 2%;
            text-align: center;
            transition: 10s;
        }

        .box input[type="text"],
        .box input[type="date"],
        .box input[type="number"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #FFB534;
            padding: 10px 10px;
            width: 350px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s
        }

        .box h1 {
            color: white;
            text-transform: uppercase;
            font-weight: 500
        }

        .box input[type="text"]:focus,
        .box input[type="date"]:focus,
        .box input[type="number"]:focus {
            width: 400px;
            border-color: #65B741
        }

        .box input[type="submit"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #65B741;
            padding: 14px 40px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
            cursor: pointer
        }

        .box input[type="submit"]:hover {
            background: #65B741
        }

        .forgot {
            text-decoration: underline;
            color: #fff
        }

        .modal {
            display: none;
            position: fixed;
            top: 0;
            right: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(to right, rgba(23, 89, 74, 2), rgba(101, 183, 65, 2));

        }

        .produit {
            max-height: 400px;
            overflow-y: auto;
        }

        .close {
            position: absolute;
            top: 10px;
            left: 10px;
            font-size: 25px;
            color: #fff;
            cursor: pointer;
        }

        .container {
            padding: 2rem 0rem;
        }

        .table-image {

            thead {

                td,
                th {
                    border: 0;
                    color: #666;
                    font-size: 0.8rem;
                }
            }

            td,
            th {
                vertical-align: middle;
                text-align: center;

                &.qty {
                    max-width: 2rem;
                }
            }
        }

        .price {
            margin-left: 1rem;
        }

        h4 {
            color: black;
        }

        .modal-footer {
            padding-top: 0rem;
        }

        .btn-success {
            border: none;
            background-color: #65B741;
            color: #fff;
            padding: 8px 15px;
            margin: 20px 0px;
            cursor: pointer;
        }

        .btn-success:hover {
            background-color: #D68910;
            color: #fff;
        }

        .btn-secondary {
            display: inline-block;
            padding: 10px 20px;
            text-decoration: none;
            color: #ffffff;
            background-color: #007bff;
            border-color: #007bff;
            border-radius: 3px;
            transition: background-color 0.3s ease;
        }

        .btn-secondary:hover {
            background-color: #0056b3;
            border-color: #0056b3;
        }

        .modal-title {
            color: black;
            font-size: 30px;
        }

    </style>
</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">
            <h1 class="logo"><a href="index.html">Thé Agro</a></h1>
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto" onclick="openModalCeuil()">Ceuilllete</a></li>
                    <li><a class="nav-link scrollto" onclick="openModalDep()">Saisie depenses</a></li>
                    <li><a class="nav-link scrollto" onclick="openModalRes()">Resultat</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

            <div id="resultat" class="modal">
                <span class="close" onclick="closeModalRes()">&times;</span>
                    <div class="container">
                        <div class="box">
                            <h1>Resultat</h1>
                            <form action="traitement.php?id=9" method="post">
                                <label>Poids totale de la cueillette</label>
                                <input type="text" name="ceuilleur" placeholder="12000" readonly>
                                <label>Poids restant sur les parcelles</label>
                                <input type="text" name="parcelle" placeholder="34" readonly>
                                <label>Cout de revient</label>
                                <input type="number" name="poids" placeholder="23000" readonly>
                            </form>
                        </div>
                    </div>
            </div>

            <script>
                function openModalRes() {
                    document.getElementById('resultat').style.display = 'block';
                }
                function closeModalRes() {
                    document.getElementById('resultat').style.display = 'none';
                }
            </script>

<div id="ceuillette" class="modal">
    <span class="close" onclick="closeModalCeuil()">&times;</span>
    <div class="container">
        <div class="box">
            <h1>Ajout Ceuilllete</h1>
            <form id="ceuilleForm" action="traitement.php?id=7" method="post">
                <input type="number" name="ceuilleur" placeholder="Choisir cueilleur">
                <input type="number" name="parcelle" placeholder="Parcelle">
                <input type="number" name="poids" id="poidsCueillette" placeholder="Poids cueilli">
                <input type="submit" value="Valider">
            </form>
        </div>
    </div>
</div>
<?php
    include('fonction.php');
?>
<script>
    $(document).ready(function() {
        $('#ceuilleForm').submit(function(e) {
            e.preventDefault();
            var poidsCueillette = $('#poidsCueillette').val();
            var poidsRestant = parseFloat("<?php echo poidsRestantParcelle(); ?>");
            if (poidsCueillette > poidsRestant) {
                alert('Le poids de la cueillette est supérieur au poids restant sur la parcelle.');
            } else {

                $('#ceuilleForm').unbind('submit').submit();
            }
        });
    });
</script>

            
            <script>
                function openModalCeuil() {
                    document.getElementById('ceuillette').style.display = 'block';
                }
                function closeModalCeuil() {
                    document.getElementById('ceuillette').style.display = 'none';
                }
            </script>
            <div id="depense" class="modal">
                <span class="close" onclick="closeModalDep()">&times;</span>
                    <div class="container">
                        <div class="box">
                            <h1>Saisie depenses</h1>
                            <form action="traitement.php?id=8" method="post">
                                <!-- <input type="date" name="date" placeholder="Date"> -->
                                <input type="number" name="depense" placeholder="Catégories de dépenses">
                                <input type="number" name="montant" placeholder="Montant">
                                <input type="submit" value="Valider">
                            </form>
                        </div>
                    </div>
            </div>

            <script>
                function openModalDep() {
                    document.getElementById('depense').style.display = 'block';
                }
                function closeModalDep() {
                    document.getElementById('depense').style.display = 'none';
                }
            </script>
        </div>
    </header>
    <main id="main">      

        <section id="parcelle" class="parcelle">
            <div class="container">

                <div class="section-title">
                    <h2>Productions et Cueillette de Thé</h2>
                    <p>Découvrez l'art de la production et de la cueillette du thé, où tradition et expertise se rencontrent pour offrir une expérience gustative incomparable. </p>
                </div>
                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <ul id="parcelle-flters">
                            <li data-filter="*">All</li>
                            <li data-filter=".filter-parcelle">Parcelle</li>
                            <li data-filter=".filter-thé">Thé</li>
                        </ul>
                    </div>
                </div>
                <div class="row parcelle-container">
                    <div class="col-lg-4 col-md-6 parcelle-item">
                        <div class="parcelle-wrap">
                            <img src="assets/img/parcelle/parcelle-01.jpg" class="img-fluid" alt="Parcelle 1">
                            <div class="parcelle-info">
                                <div class="parcelle-links">
                                    <a href="assets/img/parcelle/parcelle-01.jpg" data-gallery="parcelleGallery" class="parcelle-lightbox" title="Solide 1"><i class="bx bx-plus"></i></a>
                                    <a href="#" title="More Details"><i class="bx bx-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 parcelle-item">
                        <div class="parcelle-wrap">
                            <img src="assets/img/parcelle/parcelle-02.jpg" class="img-fluid" alt="Parcelle 1">
                            <div class="parcelle-info">
                                <div class="parcelle-links">
                                    <a href="assets/img/parcelle/parcelle-02.jpg" data-gallery="parcelleGallery" class="parcelle-lightbox" title="Solide 1"><i class="bx bx-plus"></i></a>
                                    <a href="#" title="More Details"><i class="bx bx-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
        </section>

</main>

<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-info">
                        <h3>Thé Agro</h3>
                        <p>
                            Gracia ETU2766 <br>
                            Anthony ETU2884 <br>
                            Mitia ETU2893<br><br>
                        </p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Production de thé de haute qualité</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Formation et consultation</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Visites guidées</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Vente de thé et produits dérivés</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Dégustations</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Our Newsletter</h4>
                    <p>Stay updated on our latest offerings, exclusive promotions, and exciting news by subscribing
                        to our
                        newsletter. Join now to enjoy a delightful journey of discovering fine products and special
                        offers</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                    </form>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>Thé Agro</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="#">Nom du Concepteur</a>
        </div>
    </div>
</footer>

    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</body>

</html>