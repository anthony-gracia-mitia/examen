<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Epicerie Fine - Projet S5/s3</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="assets/img/theicon.jpg" rel="icon">
    <link href="assets/img/the-icon.jpg" rel="Solidele-touch-icon">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="assets/css/styles.css" rel="stylesheet">
    <style>
        body {
            top: 20%;
            width: 100%;
            background: linear-gradient(to right, #17594A, #65B741);
            justify-content: center;
            align-items: center;

        }

        .box {
            width: 600px;
            padding: 40px;
            position: absolute;
            top: 10%;
            left: 47%;
            transform: translate(-50%, 20%);
            background: #191919;
            border-radius: 2%;
            text-align: center;
            transition: 10s;
        }

        .box input[type="text"],
        .box input[type="number"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #FFB534;
            padding: 10px 10px;
            width: 350px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s
        }

        .box h1 {
            color: white;
            text-transform: uppercase;
            font-weight: 500
        }

        .box input[type="text"]:focus,
        .box input[type="number"]:focus {
            width: 400px;
            border-color: #65B741
        }

        .box input[type="submit"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #65B741;
            padding: 14px 40px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
            cursor: pointer
        }

        .box input[type="submit"]:hover {
            background: #65B741
        }

        .forgot {
            text-decoration: underline;
            color: #fff
        }

        .modal {
            display: none;
            position: fixed;
            top: 0;
            right: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(to right, rgba(23, 89, 74, 2), rgba(101, 183, 65, 2));

        }

        .produit {
            max-height: 400px;
            overflow-y: auto;
        }

        .close {
            position: absolute;
            top: 10px;
            left: 10px;
            font-size: 25px;
            color: #fff;
            cursor: pointer;
        }

        .container {
            padding: 2rem 0rem;
        }

        .table-image {

            thead {

                td,
                th {
                    border: 0;
                    color: #666;
                    font-size: 0.8rem;
                }
            }

            td,
            th {
                vertical-align: middle;
                text-align: center;

                &.qty {
                    max-width: 2rem;
                }
            }
        }

        .price {
            margin-left: 1rem;
        }

        h4 {
            color: black;
        }

        .modal-footer {
            padding-top: 0rem;
        }

        .btn-success {
            border: none;
            background-color: #65B741;
            color: #fff;
            padding: 8px 15px;
            margin: 20px 0px;
            cursor: pointer;
        }

        .btn-success:hover {
            background-color: #D68910;
            color: #fff;
        }

        .btn-secondary {
            display: inline-block;
            padding: 10px 20px;
            text-decoration: none;
            color: #ffffff;
            background-color: #007bff;
            border-color: #007bff;
            border-radius: 3px;
            transition: background-color 0.3s ease;
        }

        .btn-secondary:hover {
            background-color: #0056b3;
            border-color: #0056b3;
        }

        .modal-title {
            color: black;
            font-size: 30px;
        }
        .box-mois{
            width: 600px;
            padding: 40px;
            position: absolute;
            top: 10%;
            left: 47%;
            transform: translate(-50%, 20%);
            background: #191919;
            border-radius: 2%;
            text-align: center;
            transition: 10s;
            color: #fff;
        }
        .box-mois input[type="checkbox"] {
            border-color: #65B741;
        }

        .box-mois input[type="submit"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #65B741;
            padding: 14px 40px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
            cursor: pointer
        }

        .box-mois input[type="submit"]:hover {
            background: #65B741; 
        }

    </style>
</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">
            <h1 class="logo"><a href="index.html">Thé Agro</a></h1>
            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto" color="white" onclick="openModalParce()">Parcelle</a></li>
                    <li><a class="nav-link scrollto" color="white" onclick="openModalCeuil()">Ceuilleur</a></li>
                    <li><a class="nav-link scrollto" color="white" onclick="openModalDep()">Catégories de dépenses</a></li>
                    <li><a class="nav-link scrollto" color="white" onclick="openModalReg()">Régénérer</a></li>
                    <li><a class="getstarted scrollto" href="index.php">Se deconnecter</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- Modal -->
            <div id="regenerer" class="modal">
                <span class="close" onclick="closeModalReg()">&times;</span>
                    <div class="container">
                        <div class="box-mois">
                            <h1>Choix des mois</h1>
                            <form action="traitement.php?id=7" method="post">
                                <div class="mois-groupe">
                                    <label class="label"><input type="checkbox" name="mois[]" value="janvier"> Janvier</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="février"> Février</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="mars"> Mars</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="avril"> Avril</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="mai"> Mai</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="juin"> Juin</label>
                                </div>
                                <div class="mois-groupe">
                                    <label class="label"><input type="checkbox" name="mois[]" value="juillet"> Juillet</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="août"> Août</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="septembre"> Septembre</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="octobre"> Octobre</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="novembre"> Novembre</label>
                                    <label class="label"><input type="checkbox" name="mois[]" value="décembre"> Décembre</label>
                                </div>
                                <input type="submit" value="Sauvegarder">
                            </form>
                        </div>
                        
                    </div>
            </div>

            <script>
                function openModalReg() {
                    document.getElementById('regenerer').style.display = 'block';
                }
                function closeModalReg() {
                    document.getElementById('regenerer').style.display = 'none';
                }
            </script>

            <div id="parcelle" class="modal">
                <span class="close" onclick="closeModalParce()">&times;</span>
                    <div class="container">
                        <div class="box">
                            <h1>Ajout Parcelle</h1>
                            <form action="traitement.php?id=3" method="post">
                                <input type="number" name="parcelle" placeholder="Numero de parcelle">
                                <input type="number" name="surface" placeholder="Surface en ha">
                                <input type="number" name="variete" placeholder="variété de thé">
                                <input type="submit" value="Enregistrer">
                            </form>
                        </div>
                    </div>
            </div>

            <script>
                function openModalParce() {
                    document.getElementById('parcelle').style.display = 'block';
                }
                function closeModalParce() {
                    document.getElementById('parcelle').style.display = 'none';
                }
            </script>

            <div id="ceuilleur" class="modal">
                <span class="close" onclick="closeModalCeuil()">&times;</span>
                    <div class="container">
                        <div class="box">
                            <h1>Ajout ceuilleur</h1>
                            <form action="traitement.php?id=4" method="post">
                                <input type="text" name="ceuilleur" placeholder="Nom ceuilleur">
                                <input type="submit" value="Enregistrer">
                            </form>
                        </div>
                    </div>
            </div>

            <script>
                function openModalCeuil() {
                    document.getElementById('ceuilleur').style.display = 'block';
                }
                function closeModalCeuil() {
                    document.getElementById('ceuilleur').style.display = 'none';
                }
            </script>

            <div id="depense" class="modal">
                <span class="close" onclick="closeModalDep()">&times;</span>
                    <div class="container">
                        <div class="box">
                            <h1>Ajout catégories de dépenses</h1>
                            <form action="traitement.php?id=5" method="post">
                                <input type="text" name="categorie" placeholder="Catégories de dépenses">
                                <input type="submit" value="Enregistrer">
                            </form>
                        </div>
                    </div>
            </div>

            <script>
                function openModalDep() {
                    document.getElementById('depense').style.display = 'block';
                }
                function closeModalDep() {
                    document.getElementById('depense').style.display = 'none';
                }
            </script>
        </div>
    </header>
    <section id="ajouter">
        <div class="container">
            <div class="box">
                <h1>Ajouter du the</h1>
                <form action="traitement.php?id=2" method="post">
                    <input type="text" name="variete" placeholder="variété de thé">
                    <input type="number" name="occupation" placeholder="Occupation">
                    <input type="number" name="rendement" placeholder="Rendement">
                    <input type="submit" value="Enregistrer">
                </form>
            </div>
        </div>
    </section>

    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>