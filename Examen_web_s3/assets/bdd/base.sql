create database the;
use the;

create table admin(
    id_admin int primary key auto_increment,
    pwd_admin varchar(20) 
);
insert into admin (pwd_admin) values ('1234');

create table variete(
    id_variete int primary key auto_increment,
    variete varchar(20),
    occupation_variete double,
    rendement_variete double
);
create table parcelle(
    id_parcelle int primary key auto_increment,
    numero_parcelle int,
    surface double,
    id_variete int
);
create table ceuilleur(
    id_ceuilleur int primary key auto_increment,
    ceuilleur varchar(20)
);
create table categorieDepense(
    id_categorieDepense int primary key auto_increment,
    categorie varchar(40)
);
create table salaire(
    id_salaire int primary key auto_increment,
    montant_salaire double,
    id_ceuilleur int,
    poids_minimum double,
    pourcentage_bonus double,
    pourcentage_mallus double
);
create table utilisateur(
    id_utilisateur int primary key auto_increment,
    email_utilisateur varchar(50),
    pwd_utilisateur varchar(20),
    nom_utilisateur varchar(20)
);
insert into utilisateur (email_utilisateur, pwd_utilisateur, nom_utilisateur) values ('user@gmail.com', '56789', 'user');
create table ceuillette(
    id_ceuillette int primary key auto_increment,
    date_ceuillette date,
    id_ceuilleur int,
    id_parcelle int,
    poids_ceuilli double
);
create table depense(
    id_depense int primary key auto_increment,
    date_depense date,
    id_categorie_depense int,
    montant double
);

CREATE VIEW vuePaiement AS
SELECT
    ceuillette.date_ceuillette AS date,
    ceuilleur.ceuilleur AS nom_ceuilleur,
    ceuillette.poids_ceuilli AS poids,
    CASE
        WHEN ceuillette.poids_ceuilli > salaire.poids_minimum THEN (salaire.montant_salaire * salaire.pourcentage_bonus / 100)
        ELSE 0
    END AS pourcentage_bonus,
    CASE
        WHEN ceuillette.poids_ceuilli < salaire.poids_minimum THEN (salaire.montant_salaire * salaire.pourcentage_mallus / 100)
        ELSE 0
    END AS pourcentage_mallus,
    (salaire.montant_salaire + (salaire.montant_salaire * salaire.pourcentage_bonus / 100) - (salaire.montant_salaire * salaire.pourcentage_mallus / 100)) AS montant_paiement
FROM
    ceuillette
JOIN
    ceuilleur ON ceuillette.id_ceuilleur = ceuilleur.id_ceuilleur
JOIN
    salaire ON ceuilleur.id_ceuilleur = salaire.id_ceuilleur;

