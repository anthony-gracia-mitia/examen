<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Production et Cueillette de thé</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="assets/img/theicon.jpg" rel="icon">
    <link href="assets/img/the-icon.jpg" rel="Solidele-touch-icon">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="assets/css/styles.css" rel="stylesheet">

    <style>
        .modal {
            display: none;
            position: fixed;
            top: 50%;
            left: 85%;
            right: 0;
            transform: translate(-50%, -50%);
            width: 35%;
            background: linear-gradient(to right, #17594A, #65B741);
            justify-content: center;
            align-items: center;

        }

        .close {
            position: absolute;
            top: 10px;
            left: 10px;
            font-size: 25px;
            color: #fff;
            cursor: pointer;
        }

        .box {
            width: 400px;
            padding: 40px;
            position: absolute;
            top: 20%;
            left: 47%;
            transform: translate(-50%, 20%);
            background: #191919;
            border-radius: 10%;
            text-align: center;
            transition: 10s;
        }

        .box input[type="mail"],
        .box input[type="password"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #FFB534;
            padding: 10px 10px;
            width: 250px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s
        }

        .box h1 {
            color: white;
            text-transform: uppercase;
            font-weight: 500
        }

        .box input[type="mail"]:focus,
        .box input[type="password"]:focus {
            width: 300px;
            border-color: #65B741
        }

        .box input[type="submit"] {
            border: 0;
            background: none;
            display: block;
            margin: 20px auto;
            text-align: center;
            border: 2px solid #65B741;
            padding: 14px 40px;
            outline: none;
            color: white;
            border-radius: 24px;
            transition: 0.25s;
            cursor: pointer
        }

        .box input[type="submit"]:hover {
            background: #65B741
        }

        .forgot {
            text-decoration: underline;
            color: #fff
        }

        ul.social-network {
            list-style: none;
            display: inline;
            margin-left: 0 !important;
            padding: 0
        }

        ul.social-network li {
            display: inline;
            margin: 0 5px
        }

        .social-network a.icoFacebook:hover {
            background-color: #3B5998;
        }

        .social-network a.icoTwitter:hover {
            background-color: #33ccff;
        }

        .social-network a.icoGoogle:hover {
            background-color: #BD3518;
        }

        .social-network a.icoInstagram:hover {
            background: linear-gradient(to right, #F39C12, #DE3163)
        }

        .social-network a.icoFacebook:hover i,
        .social-network a.icoTwitter:hover i,
        .social-network a.icoInstagram:hover i,
        .social-network a.icoGoogle:hover i {
            color: #fff
        }

        a.socialIcon:hover,
        .socialHoverClass {
            color: #44BCDD
        }

        .social-circle li a {
            display: inline-block;
            position: relative;
            margin: 0 auto 0 auto;
            border-radius: 50%;
            text-align: center;
            width: 50px;
            height: 50px;
            font-size: 20px
        }

        .social-circle li i {
            margin: 0;
            line-height: 50px;
            text-align: center
        }

        .social-circle li a:hover i,
        .triggeredHover {
            transform: rotate(360deg);
            transition: all 0.2s
        }

        .social-circle i {
            color: #fff;
            transition: all 0.8s;
            transition: all 0.8s
        }
    </style>
</head>

<body>
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center justify-content-between">

            <h1 class="logo"><a href="index.html">Thé Agro</a></h1>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a class="nav-link scrollto" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto " href="#parcelle">Parcelle</a></li>
                    <li><a class="getstarted scrollto" onclick="openModalUser()" href="#">Se connecter</a></li>
                    <li><a class="getstarted scrollto" onclick="openModalAdmin()" href="#">Admin</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>

            <!-- Modal admin login-->
            <div id="loginModalAdmin" class="modal">
                <span class="close" onclick="closeModalAdmin()">&times;</span>
                <div class="modal-content">
                    <form action="traitement.php?id=1" class="box" method="post">
                        <h1>Login</h1>
                        <p class="text-muted"> Please enter your password!</p>
                        <input type="password" name="pwd" placeholder="1234"> <a
                            class="forgot text-muted" href="#">Forgot password?</a> <input type="submit" name=""
                            value="Login" href="#">
                        <div class="col-md-12">
                            <ul class="social-network social-circle">
                                <li><a href="#" class="icoFacebook" title="Facebook"><i
                                            class="fab fa-facebook-f"></i></a>
                                </li>
                                <li><a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li><a href="#" class="icoGoogle" title="Google +"><i
                                            class="fab fa-google-plus"></i></a>
                                </li>
                                <li><a href="#" class="icoInstagram" title="Instagram"><i
                                            class="fab fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Modal utilisateur login-->
            <div id="loginModalUser" class="modal">
                <span class="close" onclick="closeModalUser()">&times;</span>
                <div class="modal-content">
                    <form action="traitement.php?id=6" method="post" class="box">
                        <h1>Login</h1>
                        <p class="text-muted"> Please enter your login and password!</p> 
                        <input type="mail" name="mail" placeholder="user@gmail.com"> 
                        <input type="password" name="pwd" placeholder="56789"> 
                        <a class="forgot text-muted" href="#">Forgot password?</a> 
                        <input type="submit" name="" value="Login" href="#">
                        <div class="col-md-12">
                            <ul class="social-network social-circle">
                                <li><a href="#" class="icoFacebook" title="Facebook"><i
                                            class="fab fa-facebook-f"></i></a>
                                </li>
                                <li><a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li><a href="#" class="icoGoogle" title="Google +"><i
                                            class="fab fa-google-plus"></i></a>
                                </li>
                                <li><a href="#" class="icoInstagram" title="Instagram"><i
                                            class="fab fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <script>
            function openModalUser() {
                document.getElementById('loginModalUser').style.display = 'block';
            }
            function closeModalUser() {
                document.getElementById('loginModalUser').style.display = 'none';
            }
            function openModalAdmin() {
                document.getElementById('loginModalAdmin').style.display = 'block';
            }
            function closeModalAdmin() {
                document.getElementById('loginModalAdmin').style.display = 'none';
            }
        </script>
        </div>
    </header>

    <section id="hero">
        <div class="hero-container">
            <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

                    <div class="carousel-item active" style="background-image: url(assets/img/slide/slide-01.jpg);">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown">Explorez les Secrets du Thé</h2>
                                <p class="animate__animated animate__fadeInUp">Découvrez les secrets de la production et de la cueillette du thé, une expérience sensorielle fascinante à explorer !</p>
                                <div>
                                    <a href="#about"
                                        class="btn-get-started animate__animated animate__fadeInUp scrollto">En
                                        savoir
                                        plus</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item" style="background-image: url(assets/img/slide/slide-02.jpg);">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown">Voyagez au Coeur du Thé</h2>
                                <p class="animate__animated animate__fadeInUp">Plongez dans l'univers envoûtant du thé, où la culture et la cueillette se rencontrent pour créer des saveurs uniques et des moments de délice.</p>
                                <div>
                                    <a href="#about"
                                        class="btn-get-started animate__animated animate__fadeInUp scrollto">En
                                        savoir
                                        plus</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item" style="background-image: url(assets/img/slide/slide-03.jpg);">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown">Au Fil des Feuilles</h2>
                                <p class="animate__animated animate__fadeInUp">Explorez notre site pour découvrir les coulisses de la production et de la cueillette du thé, où chaque feuille raconte une histoire de tradition, de savoir-faire et de passion.</p>
                                <div>
                                    <a href="#about"
                                        class="btn-get-started animate__animated animate__fadeInUp scrollto">En
                                        savoir
                                        plus</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                </a>

                <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                </a>

            </div>
        </div>
    </section>

    <main id="main">      

            <section id="parcelle" class="parcelle">
                <div class="container">

                    <div class="section-title">
                        <h2>Productions et Cueillette de Thé</h2>
                        <p>Découvrez l'art de la production et de la cueillette du thé, où tradition et expertise se rencontrent pour offrir une expérience gustative incomparable. </p>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 d-flex justify-content-center">
                            <ul id="parcelle-flters">
                                <li data-filter="*">All</li>
                                <li data-filter=".filter-parcelle">Parcelle</li>
                                <li data-filter=".filter-thé">Thé</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row parcelle-container">
                        <div class="col-lg-4 col-md-6 parcelle-item">
                            <div class="parcelle-wrap">
                                <img src="assets/img/parcelle/parcelle-01.jpg" class="img-fluid" alt="Parcelle 1">
                                <div class="parcelle-info">
                                    <div class="parcelle-links">
                                        <a href="assets/img/parcelle/parcelle-01.jpg" data-gallery="parcelleGallery" class="parcelle-lightbox" title="Solide 1"><i class="bx bx-plus"></i></a>
                                        <a href="#" title="More Details"><i class="bx bx-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 parcelle-item">
                            <div class="parcelle-wrap">
                                <img src="assets/img/parcelle/parcelle-02.jpg" class="img-fluid" alt="Parcelle 1">
                                <div class="parcelle-info">
                                    <div class="parcelle-links">
                                        <a href="assets/img/parcelle/parcelle-02.jpg" data-gallery="parcelleGallery" class="parcelle-lightbox" title="Solide 1"><i class="bx bx-plus"></i></a>
                                        <a href="#" title="More Details"><i class="bx bx-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </section>

    </main>

    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="footer-info">
                            <h3>Thé Agro</h3>
                            <p>
                                Gracia ETU2766 <br>
                                Anthony ETU2884 <br>
                                Mitia ETU2893<br><br>
                            </p>
                            <div class="social-links mt-3">
                                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Our Services</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Production de thé de haute qualité</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Formation et consultation</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Visites guidées</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Vente de thé et produits dérivés</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Dégustations</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 footer-newsletter">
                        <h4>Our Newsletter</h4>
                        <p>Stay updated on our latest offerings, exclusive promotions, and exciting news by subscribing
                            to our
                            newsletter. Join now to enjoy a delightful journey of discovering fine products and special
                            offers</p>
                        <form action="" method="post">
                            <input type="email" name="email"><input type="submit" value="Subscribe">
                        </form>

                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>Thé Agro</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                Designed by <a href="#">Nom du Concepteur</a>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/js/main.js"></script>

</body>

</html>