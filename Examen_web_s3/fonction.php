<?php
	require 'connexion.php';
	session_start();

	function verification($verifier)
	{
		$retour;
		if (isset($verifier)) 
		{
			if (!empty($verifier)) 
			{
				$retour = true;
			}
			else
			{
				$retour = false;
			}
		}
		else
		{
			$retour = false;
		}
		return $retour;
	}

//Admin
	function checkLoginAdmin($pwd)
	{
		$requete = "SELECT * FROM admin";
		$resultat = mysqli_query(bdConnect(), $requete);
		$reponse = false;
		while ($donne = mysqli_fetch_assoc($resultat)) 
		{
			if($donne['pwd_admin'] == $pwd)
			{
				$reponse = true;
				$_SESSION['idAdmin'] = $donne['id_admin'];
			}
			else
			{
				$reponse = false;
			}
		}
		if ($reponse == true) 
		{
			header('Location: admin.php');
		}
		else
		{
            $message = "Erreur lors de la connexion de l'admin";
            $_SESSION['messageAdminErreur'] = $message;
            header('Location: index.php');
		}
	}

	function insererVariete($variete, $occupation_variete, $rendement_variete) 
	{
		if (verification($variete) == true && verification($occupation_variete) == true && verification($rendement_variete) == true) 
		{
			$connexion = bdConnect();
		    $requete = "INSERT INTO variete (variete, occupation_variete, rendement_variete) VALUES ('$variete', $occupation_variete, $rendement_variete)";
		    if (mysqli_query($connexion, $requete)) 
		    {
		        $message = "Les données ont été insérées avec succès.";
		        $_SESSION['messageVariete'] = $message;
		    } 
		    else 
		    {
		        $message = "Erreur lors de l'insertion des données : " . mysqli_error($connexion);
		        $_SESSION['messageVariete'] = $message;
		    }			
		}
		header('Location: admin.php');
	}

	function insererParcelle($numero_parcelle, $surface, $id_variete) 
	{
	    if (verification($numero_parcelle) == true && verification($surface) == true && verification($id_variete) == true) 
	    {
	        $connexion = bdConnect();
	        $requete = "INSERT INTO parcelle (numero_parcelle, surface, id_variete) VALUES ($numero_parcelle, $surface, $id_variete)";
	        if (mysqli_query($connexion, $requete)) 
	        {
	            $message = "Les données du Parcelle ont été insérées avec succès.";
	            $_SESSION['messageParcelle'] = $message;
	        } 
	        else 
	        {
	            $message = "Erreur lors de l'insertion des données du Parcelle: " . mysqli_error($connexion);
	            $_SESSION['messageParcelle'] = $message;
	        }			
	    }
	    header('Location: admin.php');
	}

	function insererCeuilleur($ceuilleur) 
	{
	    if (verification($ceuilleur) == true) 
	    {
	        $connexion = bdConnect();
	        $requete = "INSERT INTO ceuilleur (ceuilleur) VALUES ($ceuilleur)";
	        if (mysqli_query($connexion, $requete)) 
	        {
	            $message = "Les données du Ceuilleur ont été insérées avec succès.";
	            $_SESSION['messageParcelle'] = $message;
	        }
	        else 
	        {
	            $message = "Erreur lors de l'insertion des données du Ceuilleur: " . mysqli_error($connexion);
	            $_SESSION['messageParcelle'] = $message;
	        }			
	    }
	    header('Location: admin.php');
	}

	function insererCategorieDepense($categorie) 
	{
		if (verification($categorie)) 
		{

			$connexion = bdConnect();
			if ($connexion) 
			{
				$requete = "INSERT INTO categorieDepense (categorie) VALUES (?)";
				$statement = mysqli_prepare($connexion, $requete);
				mysqli_stmt_bind_param($statement, 's', $categorie);
	
				if (mysqli_stmt_execute($statement)) 
				{
					$message = "Les données de la catégorie ont été insérées avec succès.";
					$_SESSION['messageParcelle'] = $message;
				} 
				else 
				{
					$message = "Erreur lors de l'insertion des données de la catégorie: " . mysqli_stmt_error($statement);
					$_SESSION['messageParcelle'] = $message;
				}
			} 
			else 
			{
				$message = "Erreur de connexion à la base de données.";
				$_SESSION['messageParcelle'] = $message;
			}
		} 
		else 
		{
			$message = "La catégorie de dépense n'est pas valide.";
			$_SESSION['messageParcelle'] = $message;
		}

		header('Location: admin.php');
		// echo $message;
	}
	

	function salaireCeuilleur($poids,$salaireParKg)
	{
		$salaire = $poids * $salaireParKg;
		return $salaire;
	}
	
//Utilisateur
	function checkLoginUtilisateur($email,$pwd)
	{
		$requete = "SELECT * FROM utilisateur";
		$resultat = mysqli_query(bdConnect(), $requete);
		$reponse = false;
	    if (verification($email) == true && verification($pwd) == true) 
	    {
			while ($donne = mysqli_fetch_assoc($resultat))
			{
				if($donne['email_utilisateur'] == $email && $donne['pwd_utilisateur'] == $pwd)
				{
					$reponse = true;
					$_SESSION['idUtilisateur'] = $donne['id_utilisateur'];
				}
				else
				{
					$reponse = false;
				}
			}
			if ($reponse == true) 
			{
				header('Location: utilisateur.php');
			}
			else
			{
	            $message = "Veuillez verifier votre Mot de passe ou votre Email";
	            $_SESSION['messageUtilisateurErreur'] = $message;
	            header('Location: index.php');
			}
		}
		else
		{
			$message = "Veuillez completer le formulaire";
            $_SESSION['messageUtilisateurErreur'] = $message;
			header('Location: index.php');
		}
	}

	function insertCueillettes($choixIdCueilleur,$choixIdParcelle,$poidsCueilli)
	{
		$connexion = bdConnect();
	    if (
	    		verification($choixIdCueilleur) == true ||
	    		verification($choixIdParcelle) == true ||
	    		verification($poidsCueilli) == true
	       ) 
	    {
	    	$requete = "INSERT INTO ceuillette (date_ceuillette,id_ceuilleur,id_parcelle,poids_ceuilli) VALUES (now(),$choixIdCueilleur,$choixIdParcelle,$poidsCueilli)";
	 		// echo $requete;
			$resultat = mysqli_query($connexion, $requete);
			header('Location: utilisateur.php');
		}
		else
		{
			$message = "Veuillez completer le formulaire";
            $_SESSION['messageUtilisateurErreur'] = $message;
			header('Location: utilisateur.php');
			
		}
		echo $message;
	}

	function insertDepense($choixCategorieDepense, $montant)
	{
		$connexion = bdConnect();
	    if (
	    		verification($choixCategorieDepense) == true && 
	    		verification($montant) == true
	       ) 
	    {
	    	$requete = "INSERT INTO depense (date_depense,id_categorie_depense,montant) VALUES (now(),$choixCategorieDepense,$montant)";
	 		$resultat = mysqli_query($connexion, $requete);
		}
		else
		{
			$message = "Veuillez completer le formulaire";
            $_SESSION['messageUtilisateurErreur'] = $message;
			header('Location: nomDePage.php');
		}
	}

	function calculTotalCeuillette($idUtilisateur)
	{
	    $connexion = bdConnect();
	    $requete = "SELECT SUM(poids_ceuilli) AS poidsTotal 
	                FROM ceuillette 
	                WHERE id_utilisateur = $idUtilisateur";
	    $resultat = mysqli_query($connexion, $requete);
	    $donnees = mysqli_fetch_assoc($resultat);
	    $totalCeuillette = $donnees['poidsTotal'];
	    return $totalCeuillette;
	}

	function poidsRestantParcelle()
	{
	    $connexion = bdConnect();
	    $idUtilisateur = $_SESSION['idUtilisateur'];
	    $requete = "SELECT SUM(ceuillettes.poids_ceuillette) - SUM(varieteParcelle.rendement) AS restePoidsParcelle 
	                FROM varieteParcelle 
	                JOIN ceuillettes ON ceuillettes.id_parcelle = varieteParcelle.id_parcelle 
	                WHERE ceuillettes.id_utilisateur = $idUtilisateur";
	    $resultat = mysqli_query($connexion, $requete);
	        $donnees = mysqli_fetch_assoc($resultat);
	        $restePoidsParcelle = $donnees['restePoidsParcelle'];
	        return $restePoidsParcelle;
	}

	function selectVuePaiement() {
		$connexion = bdConnect();
		
		$requete = "SELECT * FROM VuePaiement";

		$resultat = $connexion->query($requete);

		if ($resultat->num_rows > 0) {
			$tableau_html = "<table border='1'><tr><th>Date</th><th>Nom cueilleur</th><th>Poids</th><th>% Bonus</th><th>% Mallus</th><th>Montant paiement</th></tr>";

			while ($row = $resultat->fetch_assoc()) {
				$tableau_html .= "<tr><td>".$row['date']."</td><td>".$row['nom_cueilleur']."</td><td>".$row['poids']."</td><td>".$row['pourcentage_bonus']."</td><td>".$row['pourcentage_mallus']."</td><td>".$row['montant_paiement']."</td></tr>";
			}

			$tableau_html .= "</table>";

			return $tableau_html;
		} else {
			return "Aucune donnée trouvée dans la vue.";
		}
	}
?>