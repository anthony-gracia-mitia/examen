<?php

    require 'fonction.php';
    $idTraitement = $_GET['id'];

    if($idTraitement == 1)
    {
        $pwd = $_POST['pwd'];
        checkLoginAdmin($pwd);
    }

    if($idTraitement == 2)
    {
        $variete = $_POST['variete'];
        $occupation = $_POST['occupation'];
        $rendement = $_POST['rendement'];
        insererVariete($variete, $occupation, $rendement);
    }

    if($idTraitement == 3)
    {
        $variete = $_POST['variete'];
        $parcelle = $_POST['parcelle'];
        $surface = $_POST['surface'];
        insererParcelle($parcelle, $surface, $variete);
    }

    if($idTraitement == 4)
    {
        $ceuilleur = $_POST['ceuilleur'];
        insererCeuilleur($ceuilleur);
    }

    if($idTraitement == 5)
    {
        $categorie = $_POST['categorie'];
        insererCategorieDepense($categorie);
    }

    if($idTraitement == 6)
    {
        $email = $_POST['mail'];
        $pwd = $_POST['pwd'];
        checkLoginUtilisateur($email,$pwd);
    }

    if ($idTraitement == 7) 
    {
        $ceuilleur = $_POST['ceuilleur'];
        $parcelle = $_POST['parcelle'];
        $poids = $_POST['poids'];
        insertCueillettes($ceuilleur,$parcelle,$poids);
    }

    if ($idTraitement == 8) 
    {
        $depense = $_POST['depense'];
        $montant = $_POST['montant'];
        insertDepense($depense, $montant);
    }

    if ($idTraitement == 10) {
        
    }
?>